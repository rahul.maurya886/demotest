package com.service.impl;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.service.DemoService;
import com.service.constants.ExampleConstants;

@Service
public class DemoServiceImpl implements DemoService{
	
	
	@Override
	public String getName()
	{
		String name=ExampleConstants.NAME+" "+ExampleConstants.SURNAME;
		return name;
		
	}
}
