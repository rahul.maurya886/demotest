package com.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.DemoService;

@RestController
public class DemoController 
{

	@Autowired
	private DemoService demoservice;
	
	@RequestMapping(value="/getName", method=RequestMethod.GET)
	public ResponseEntity<String> getEmpName()
	{
		 
		String name=demoservice.getName();
		return new ResponseEntity<String>(name,HttpStatus.OK);
		
	}
}
