package com.example.demo;

import org.junit.Test;
import static org.mockito.Mockito.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import com.example.service.DemoService;
import com.service.impl.DemoServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ServiceTest {

	@InjectMocks
	private static DemoServiceImpl demoservice;
	

	@Test
	public void testName()
	{
		String name=demoservice.getName();
		assertEquals("RAHUL","RAHUL");
		
	}
	
}
